<?php
/**
 * MANUAL MOIP SDK https://github.com/moip/moip-sdk-php
 * MANUAL WHMCS https://github.com/WHMCS/sample-merchant-gateway/blob/master/modules/gateways/merchantgateway.php
 MAIS INFOS: https://developers.whmcs.com/payment-gateways/merchant-gateway/
 */

require __DIR__.'/moipcc/sdk/vendor/autoload.php';
use Moip\Moip;
use Moip\Auth\BasicAuth;


/*
PARA INSTALAR:
nao esquecer de criar os campos personalizados, 
*/


if (!defined("WHMCS")) {
    die("This file cannot be accessed directly");
}
/**
 * Define module related meta data.
 *
 * Values returned here are used to determine module related capabilities and
 * settings.
 *
 * @see https://developers.whmcs.com/payment-gateways/meta-data-params/
 *
 * @return array
 */
function moipcc_MetaData()
{
    return array(
        'DisplayName' => 'Moip Credit Card WHMCS',
        'APIVersion' => '1.1', // Use API Version 1.1
        'DisableLocalCreditCardInput' => false,
        'TokenisedStorage' => false,
    );
}
/**
 * Define gateway configuration options.
 *
 * The fields you define here determine the configuration options that are
 * presented to administrator users when activating and configuring your
 * payment gateway module for use.
 *
 * Supported field types include:
 * * text
 * * password
 * * yesno
 * * dropdown
 * * radio
 * * textarea
 *
 * Examples of each field type and their possible configuration parameters are
 * provided in the sample function below.
 *
 * @see https://developers.whmcs.com/payment-gateways/configuration/
 *
 * @return array
 */


function moipcc_config()
{

    return array(
        // the friendly display name for a payment gateway should be
        // defined here for backwards compatibility
        'FriendlyName' => array(
            'Type' => 'System',
            'Value' => 'Moip Credit Card WHMCS',
        ),
        // a text field type allows for single line text input
        'api_identifier_whmcs' => array(
            'FriendlyName' => 'API Identificador WHMCS',
            'Type' => 'text',
            'Size' => '40',
            'Default' => '',
            'Description' => 'Acesso WHMCS identificador',
        ),
        // a password field type allows for masked text input
        'api_secret_whmcs' => array(
            'FriendlyName' => 'API Secret Key WHMCS',
            'Type' => 'text',
            'Size' => '45',
            'Default' => '',
            'Description' => 'Acesso WHMCS secret, criar em Options -> Staff Management -> Manage API Credentials',
        ),
        // a text field type allows for single line text input
        // the yesno field type displays a single checkbox option
        'moipAtivo' => array(
            'FriendlyName' => 'Moip Producao',
            'Type' => 'yesno',
            'Description' => 'Clique para habilitar MOIP em produção',
        ),
        'accountID' => array(
            'FriendlyName' => 'MOIP Token',
            'Type' => 'text',
            'Size' => '40',
            'Default' => '',
            'Description' => 'Moip token',
        ),
        // a password field type allows for masked text input
        'secretKey' => array(
            'FriendlyName' => 'Moip Secret Key',
            'Type' => 'text',
            'Size' => '45',
            'Default' => '',
            'Description' => 'Enter Moip secret key here',
        ),
        // a password field type allows for masked text input
        'key_whmcs' => array(
            'FriendlyName' => 'Moip WHMCS Key',
            'Type' => 'text',
            'Size' => '45',
            'Default' => '',
            'Description' => 'Moip Key URL validator callback',
        ),
        // a password field type allows for masked text input
        'moipIDAdmin' => array(
            'FriendlyName' => 'MOIP ID CUSTOMER',
            'Type' => 'dropdown',
            'Options' =>get_customfield_id(),
            'Description' => 'Create a custom field with name moipID and type the array number here',
        ),
        'moipCCAdmin' => array(
            'FriendlyName' => 'MOIP CC',
            'Type' => 'dropdown',
            'Options' =>get_customfield_id(),
            'Description' => 'Create a custom field with name moipCC and type the array number here',
        ),
        'birthday' => array(
            'FriendlyName' => 'Birthday Date',
            'Type' => 'dropdown',
            'Options' =>get_customfield_id(),
            'Description' => 'Birthday date type the array number here',
        ),
        'cpfMoip' => array(
            'FriendlyName' => 'CPF',
            'Type' => 'dropdown',
            'Options' =>get_customfield_id(),
            'Description' => 'CPF type the array number here'
        ),
        'cnpj' => array(
            'FriendlyName' => 'CNPJ Data',
            'Type' => 'dropdown',
            'Options' =>get_customfield_id(),
            'Description' => 'CNPJ type the array number here',
        ),
    );
}



/// METODO PARA ADICIONAR OU REMOVER UM CARTÃO DE CRÉDITO
///https://developers.whmcs.com/payment-gateways/tokenised-remote-storage/
//function moipcc_storeremote($params){
////////////// TODO O CLIENTE EU SALVO NO MOIP APENAS O ID DO MOIP DO CARTAO DE CREDITO Q VEM PRA CAH
//} // habilitado apenas para não salvar os dados de cartão local



function moipcc_capture($params){
    //echo"CPFww".$params["clientdetails"]["customfields15"]["15"];

    require __DIR__.'/moipcc/params.php';
    
    //if(empty($_POST['cccvv'])){
         $moipCVC = $params['cccvv'];
    //}else{
        //$moipCVC = $_POST['cccvv'];
    //}

    ///SALVAR USUARIO NO MOIP VALIDAR DADOS DE CADASTRO.
    if(empty($moipIDSalvo)){
        //echo "salvarCustomer";
        $moipID = salvarCustomer($params);// retorna o ID do CUSTOMER ou false
        //echo $moipID;

        if ($moipID != false){
            ///////////////////////////////////////////// testar
            $moipCC = salvaCartaoCredito($params, $moipID);
      
            if($moipCC != false){
                $cartaoSalvo = salvarCartaoIntoCustomer($params, $userID, $moipID, $moipCC);
                if($cartaoSalvo){
                    //echo "pagar".$moipID;
                    //print_r($params);
                    /*
                    Pagar agora 

                    [paymentmethod] => moipcc 
                    [whmcsVersion] => 7.5.1-release.1 
                    [accountID] => PWEDPRAAGYTP0QXR8G0KXLCGRYCKRKXH 
                    [api_identifier_whmcs] => wIzOdDxb54Ittk35pYuUeeJRPX0paeNt 
                    [api_secret_whmcs] => redwnD2MHcgbct5Zs26YqaIfYEr9cLVF 
                    [birthday] => 4 
                    [cnpj] => 1 
                    [convertto] => 
                    [cpf] => 0 
                    [dropdownField] => option1 
                    [key_whmcs] => 159 
                    [moipCCAdmin] => 3 
                    [moipIDAdmin] => 2 
                    [name] => Cartao de Credito MOIP 
                    [radioField] => 
                    [secretKey] => KDAGBSIHBFLLVSQRKOFGHYJCHU7B6YTNXSSONG7R 
                    [testMode] => on 
                    [textareaField] => 
                    [type] => Invoices 
                    [visible] => on 
                    [invoiceid] => 7 
                    [invoicenum] => 7 
                    [amount] => 25.60 
                    [description] => Link DEV - Fatura #7 [returnurl] => https://www.linknacional.com.br/dev/viewinvoice.php?id=7 [dueDate] => 2018-06-04 00:00:00 [clientdetails] => Array ( [userid] => 1 [id] => 1 [firstname] =>
                    */

                    $pagamento = pay($params, $moipID, $moipCC, $moipCVC);

                    $reflector = new \ReflectionClass($pagamento);
                    $classProperty = $reflector->getProperty('data');
                    $classProperty->setAccessible(true);
                    $data = $classProperty->getValue($pagamento);

                    if($pagamento != false){
                        //// VERIFICAR SE PRECISA DELETAR DADOS DO CARTAO
                        if($_POST['nostore'] == true){
                            deletarCartao($params);
                        }
                        $key = $key_whmcs;
                        require_once __DIR__.'/callback/moipcc.php';
                        ///// TESTAR CONFIRMAçÔES DE pagamento
                    }
                }
            }
        }
///////////////////////DAQ PRA BAIXO VERIFICAR AS CONDICOES SEGUNDA PARTE DO DESENVOLVIMENTO
    }elseif(!empty($moipIDSalvo) && empty($pagamento)){
        //// VALIDAR SE CLIENTE EXISTE
        //echo "Buscar customer".$moipID;
        $customerDetail = buscarCustomerMoip($params, $moipIDSalvo);
        //print_r($customerDetail);
        //echo "TEM Cartao:".$moipCC;

        // CUSTOMER ESTA CORRETO
        if($customerDetail != false){

            // NOVO CARTAO DE CREDITO
            if($_POST["ccinfo"] == "new"){
                $moipCC = salvaCartaoCredito($params, $moipIDSalvo);

                if ($moipCC != false){
                    $resultadoSave = salvarCartaoIntoCustomer($params, $userID, $moipIDSalvo, $moipCC);
                    /////////// Ficar atento caso o usuario selecionar a opção de nao querer salvar o cartao de crédito.
                    /// TEM VERIFICAR E FAZER FUNCAO DE DELETAR CARTAO SE TIVER AQUI
                    if($resultadoSave == true){
                        //echo "pay";
                        $pagamento = pay($params, $moipIDSalvo, $moipCC, $moipCVC);
                        if($pagamento != false){
                            //// VERIFICAR SE PRECISA DELETAR DADOS DO CARTAO
                            if($_POST['nostore'] == true){
                                deletarCartao($params);
                            }
                            $key = $key_whmcs;
                            require_once __DIR__.'/callback/moipcc.php';
                        }
                    }
                }
            }

            // USAR CARTAO JÁ CADASTRADO
            if($_POST["ccinfo"] == "useexisting"){
                $pagamento = pay($params, $moipIDSalvo, $moipCCSalvo, $moipCVC);
                if($pagamento != false){
                    $key = $key_whmcs;
                    require_once __DIR__.'/callback/moipcc.php';
                }
            }
            if(empty($_POST["ccinfo"])){
                echo "CCINFO NÄO DEFINIDO";
            }
        }
    }
}

//// METODO PARA DELETAR O CARTAO DO MOIP 
function deletarCartao($params){
    require __DIR__.'/moipcc/params.php';
    $moip = conectaMOIP($params);
    $moip->customers()->creditCard()->delete($moipCCSalvo);
}

function get_customfield_id() {
    $fields = mysql_query("SELECT id, fieldname FROM tblcustomfields WHERE type = 'client';");
    if (!$fields) {
        return array('0' => 'database error');
    }elseif (mysql_num_rows($fields) >= 1) {
        $dropFieldArray = array();
        while ($field = mysql_fetch_assoc($fields)) {
        // the dropdown field type renders a select menu of options
        $dropFieldArray[$field['id']] = $field['fieldname'];
        }
       return $dropFieldArray;
    } else {
        return array('0' => 'nothing to show');
    }
}

/// METODO PARA SALVAR O CLIENTE NO MOIP, RETORNA O ID DO CUSTOMER DO MOIP
function salvarCustomer($params){
    
    require __DIR__.'/moipcc/params.php';
    //echo  "nome". $firstname .  "sobrenome". $lastname  .$email ."nive".$birthday . "cpf". $cpfStr. $phoneDD. "Salvar Customer:".   $phoneSufixo . $street . $homeNumber . $city . $state   ."sufixo" .$postcode .$params['clientdetails']['phonenumber'];

    $erro = false;

    //echo "CPF".$cpf;

    if(empty($firstname) || empty($lastname)){
        $msg = "Favor preencher sou nome e sobrenome corretamente";
        $erro =  true;
    }elseif (empty($email)) {
         $msg .=  "<br>Favor preencher seu email corretamente";
         $erro =  true;
    }elseif (empty($birthday)) {
         $msg .= "<br>Favor preencher sua data de aniversário corretamente";
         $erro =  true;
    }elseif (empty($cpf)) {
         $msg .= "<br>Favor preencher os dados do CPF corretamente";
         $erro =  true;
    }elseif (empty($phoneDD)|| empty($phoneSufixo)) {
         $msg .= "<br>Favor preencher seu telefone corretamente";
         $erro =  true;
    }elseif (empty($street) || empty($city) || empty($homeNumber) || empty($state) || empty($postcode)) {
         $msg .= "<br>Favor preencher seu endereço corretamente";
         $erro =  true;
    }

    if($erro){
        echo "<div class='alert alert-danger'> $msg. <br>Corrigir os erros de dados cadastrais antes de prosseguir com seu pagamento. <a href='clientarea.php?action=details'>Editar</a></div>";
        return false;
        die();
    }

    if($erro == false){

            $moip = conectaMOIP($params);

        try {
            $customer_moip = $moip->customers()->setOwnId(uniqid())
                ->setFullname($firstname . ' ' . $lastname)
                ->setEmail($email)
                ->setBirthDate($birthday)
                ->setTaxDocument($cpf)
                ->setPhone($phoneDD,$phoneSufixo)
                ->addAddress('SHIPPING',
                    $street , $homeNumber   ,
                    'Bairro', $city, $state,$postcode, 8)
                ->create();
            // Acessando o ID CUSTOMER
            $reflector = new \ReflectionClass($customer_moip);
            $classProperty = $reflector->getProperty('data');
            $classProperty->setAccessible(true);
            $data = $classProperty->getValue($customer_moip);

            //// retorna o ID do CUSTOMER DO MOIP
            return $data->id;

            } catch (\Moip\Exceptions\UnautorizedException $e) {
                echo $e->getMessage();
            }
    }
}

    // Pesquisar cliente no MOIP pelo ID salvo no perfil do WHMCS
    function buscarCustomerMoip($params, $moipID){
        require __DIR__.'/moipcc/params.php';
        $moip = conectaMOIP($params);

        try{
            $customerMoip = $moip->customers()->get($moipID);
            return $customerMoip;
        } catch (\Moip\Exceptions\UnautorizedException $e) {
            echo $e->getMessage();
            return false;
        }
    }

    // buscar objeto cartao no moip NAO EXISTE
    function buscarCCMoip($moipCC){
        require __DIR__.'/moipcc/params.php';

        $moip = conectaMOIP($params);

        try{
            $moipCC = $moip->customers()->creditCard()->get($moipCC);
            return $moipCC;
        } catch (\Moip\Exceptions\UnautorizedException $e) {
            echo $e->getMessage();
            return false;
        }
    }

    function conectaMOIP($params){
        require __DIR__.'/moipcc/params.php';

        if($moipTeste){
            $moip = new Moip(new BasicAuth($token_moip, $key_moip), Moip::ENDPOINT_SANDBOX);
        }elseif($moipTeste == false){
            $moip = new Moip(new BasicAuth( $token_moip, $key_moip), Moip::ENDPOINT_PRODUCTION);
        }
        return $moip;
    }

    // Adicionar Cartão de Crédito no Cliente           /// OBTER OS DADOS DO CARTAO
    function salvaCartaoCredito($params, $moipID){
        require __DIR__.'/moipcc/params.php';
        
        $moip = conectaMOIP($params);
        
        try{
        $customer = $moip->customers()->creditCard()
            ->setExpirationMonth($_POST['ccexpirymonth'])
            ->setExpirationYear($_POST['ccexpiryyear'])
            ->setNumber($_POST['ccnumber'])
            ->setCVC($_POST['cccvv'])
            ->setFullName($_POST['firstname']. ' '.$_POST['lastname'])
            ->setBirthDate($birthday)
            ->setTaxDocument('CPF',  $cpf)
            ->setPhone($phoneDD,$phoneSufixo) //->setPhone($phoneDD,$phoneSufixo,$country) 
            ->create($moipID);

            // Acessando o ID CUSTOMER
            $reflector = new \ReflectionClass($customer);
            $classProperty = $reflector->getProperty('data');
            $classProperty->setAccessible(true);
            $cartao_id = $classProperty->getValue($customer);


            return $cartao_id->creditCard->id;//retorna todos os dados do cartão $cartao_id->creditCard
            //// Retornar o ID do cartão de credito
        } catch (\Moip\Exceptions\UnautorizedException $e) {
            echo $e->getMessage();
            return false;
        }
}

/// Atualizando o cliente no WHMCS com o customer id do moip e a tag do cartão de crédito.
function salvarCartaoIntoCustomer($params, $userID, $moipID, $moipCC){
    //echo "salvarCartaoIntoCustomer";
    require __DIR__.'/moipcc/params.php';

        // LOGANDO NO WHMCS
        $customfields = array($moipIDAdmin => $moipID, $moipCCAdmin => $moipCC );

        // Atualizando Cliente nos custom fields
        $postfields = array(
            'identifier' => $api_identifier_whmcs,
            'secret' => $api_secret_whmcs,
            'clientid' => $userID,
            'customfields' => base64_encode(serialize($customfields)),
            'action' => 'updateclient',
            'skipvalidation' => true,
            'responsetype' => 'json'
        );

        // Call the API
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $systemUrl . 'includes/api.php');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);
        // Attempt to decode response as json
        $jsonData = json_decode($response, true);

        // CLIENTE ATUALIZADO
        if($jsonData['result'] == "success"){
            return true;
        }else{
            return false;
            //print_r($jsonData);
            echo "NAO FOI POSSIVEL ATUALIZAR CLIENTE COM OS DADOS";
        }
}

function pay($params, $moipID, $moipCC, $moipCVC){
    //echo "pagando";
    require __DIR__.'/moipcc/params.php';
    $moip = conectaMOIP($params);

    $customerMoip = buscarCustomerMoip($params, $moipID);

    $amountDot = $params['amount'];
    $totalTratado = str_replace(".", "", $amountDot);

    try{
        $order = $moip->orders()->setOwnId($invoiceID.":".uniqid())
            ->addItem($params['description'], 1, $invoiceID, (int)$totalTratado)
            ->setCustomer($customerMoip)
            ->create();
            $payment = $order->payments()->setCreditCardSaved($moipCC, $moipCVC)
    ->execute();
/*
        $notification1 = $moip->notifications()->addEvent('ORDER.*')
        ->addEvent('PAYMENT.IN_ANALYSIS')//->addEvent('PAYMENT.AUTHORIZED') removi o *
        ->setTarget($systemUrl.'/modules/gateways/callback/moipcc.php?key='.$key_whmcs)
        ->create();
*/
        //https://dev.moip.com.br/v2.0/reference#eventos-mp
        $notification = $moip->notifications()
        ->addEvent('ORDER.PAID')
        ->addEvent('ORDER.NOT_PAID')
        //->addEvent('PAYMENT.CANCELLED')
       // ->addEvent('PAYMENT.IN_ANALYSIS')
        //->addEvent('PAYMENT.AUTHORIZED')
        ->setTarget($systemUrl.'/modules/gateways/callback/moipcc.php?key='.$key_whmcs)
        ->create();

        return $payment;

        } catch (\Moip\Exceptions\UnautorizedException $e) {
            echo $e->getMessage();
            return false;
        }
}