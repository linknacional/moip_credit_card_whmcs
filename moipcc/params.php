<?php
/**
 * Módulo MOIP Cartão para WHMCS
 * @author		Davi Souza | linknacional.com.br
 * @see			https://linknacional.com.br
 * @copyright	2017 https://linknacional.com.br
 * @license		https://www.gnu.org/licenses/gpl-3.0.pt-br.html
 * @support		https://linknacional.com.br/suporte
 * @version		1.0.0
 */

foreach( glob( __DIR__.'/params/*.php') as $file ) {
       include $file;
}