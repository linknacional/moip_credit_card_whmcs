 <?php
 /**
 * Módulo MOIP Cartão para WHMCS
 * @author      Davi Souza | linknacional.com.br
 * @see         https://linknacional.com.br
 * @copyright   2017 https://linknacional.com.br
 * @license     https://www.gnu.org/licenses/gpl-3.0.pt-br.html
 * @support     https://linknacional.com.br/suporte
 * @version     1.0.0
 */


// Parametros da Fatura
$invoiceID              = $params['invoiceid'];
$invoiceDescription     = $params["description"];
$invoiceAmount          = $params["amount"];

 // PARAMETROS DE CONFIGURAÇÕES
    
    //// OBTER numero da ordem dos campos personalizados
    $moipIDAdmin = $params['moipIDAdmin'];
    $moipCCAdmin = $params['moipCCAdmin']; 
    $key_whmcs = $params['key_whmcs']; 

    //echo "ID". $moipIDAdmin.'CC'.  $moipCCAdmin;

    $systemUrl = $params['systemurl'];
    $userID =  $params["clientdetails"]["userid"];
    $moipCVC = $params['cccvv'];
    $token_moip = $params['accountID']; //"";
    $key_moip = $params['secretKey']; //""; 
    $api_identifier_whmcs = $params['api_identifier_whmcs'];//'ZiHI2gSytqpdAGZwVu9tU7MLEHVeWEdS';
    $api_secret_whmcs = $params['api_secret_whmcs']; //'ZiHI2gSytqpdAGZwVu9tU7MLEHVeWEdS';
    if($params['moipAtivo'] =="on"){
        $moipTeste = false;
    }else{
        $moipTeste = true;
    }
//$moipTeste = true;
    //$moipTeste = false;/// MODO TESTE HABILITADO
    //print_r($params["clientdetails"]["customfields2"]);
//7 cnpj
//5 tipo pessoa
//2 moipcc
//1 whatsapp
//0 CPF



$myclientcustomfields = array();
foreach($params["clientdetails"]["customfields"] as $key => $value){
$myclientcustomfields[$value['id']] = $value['value'];
}

$moipIDSalvo =   $myclientcustomfields[$params['moipIDSalvo']];
$moipCCSalvo =    $myclientcustomfields[$params['moipCCSalvo']];
$customfbirthday =  $myclientcustomfields[$params['birthday']]; 


//["cpf" => $params['cpfMoip'],
/*
$array[] = $var;

key 0 value Array
Array ( 
    [id] => 2 
    [value] => 22272538860 )
*/
/*

print_r($params["clientdetails"]["customfields2"]);

Array ( 
    [0] => Array ( 
        [id] => 2 
        [value] => 22272538860 ) 
    [1] => Array ( 
        [id] => 15 
        [value] => 16996113223 ) 
    [2] => Array ( 
        [id] => 19 
        [value] => ) 
    [3] => Array ( 
        [id] => 7 
        [value] => Masculino ) 



$customfbirthday   = $params['customfieldbirthday'];
$birthday_pre    = preg_replace('/[^\da-z]/i', '', $params["clientdetails"]["customfields"]["$customfbirthday"]["value"]);
*/
/*
$array = $params["clientdetails"]["customfields"];//[[id => 2, value = 22272538860],[id => 15, value = 16996113223]];
$customFieldsMoip = ["cpf" => $params['cpfMoip'],"cnpj" => $params['cnpj'],"birthday" => $params['birthday'], "moipIDSalvo" => $params['moipIDAdmin'], "moipCCSalvo" => $params['moipCCAdmin']];
$newVet = [];


foreach($array as $val){
 foreach($customFieldsMoip as $kPar=>$valPar){
    if($val["id"] == $valPar){
        //$newVet[$kPar] = $valPar["value"];
        array_push($newVet,[$kPar => $val["value"]]);
    }
 }
}


Array ( 
    [0] => Array ( 
        [cpf] => 22272538860 ) 
    [1] => Array ( 
        [moipCCSalvo] => ) 
    [2] => Array ( 
        [birthday] => 08/07/1981 ) 
    [3] => Array ( 
        [cnpj] => 013682434000160 ) 
    [4] => Array ( [moipIDSalvo] => ) ) 

niover

Array ( [0] => Array ( [cpf] => 22272538860 ) [1] => Array ( [moipCCSalvo] => ) [2] => Array ( [birthday] => 08/07/1981 ) [3] => Array ( [cnpj] => 013682434000160 ) [4] => Array ( [moipIDSalvo] => ) ) 


$moipIDSalvo =   $newVet['moipIDSalvo'];
$moipCCSalvo =   $newVet['moipCCSalvo'];
$customfbirthday = $newVet['birthday'];  
echo "niover". $newVet['birthday'];

print_r($newVet);
//print_r($params);

*/

//$params["clientdetails"]["customfields"][];


//echo"CPF".$params["clientdetails"]["customfields"]["0"]["value"]["15"];
//print_r($params["clientdetails"]['customfields']);


//$customfCpf1 = $params['customfieldscpf'];
//print_r($params["clientdetails"]["customfields"]["$customfCpf1"]["value"]);

//print_r($params["clientdetails"]["customfields2"]);
//print_r($params["clientdetails"]["customfields"][0]['value']);


//echo "moipIDSalvo".$moipIDSalvo;/
//echo "moipCCSalvo".$moipCCSalvo;
    // parametros do cartão de CREDITO

 // Parametros do Cliente
    $userID                 = $params['clientdetails']['id'];
    $firstname              = $params['clientdetails']['firstname'];
    $lastname               = $params['clientdetails']['lastname'];
    $email                  = $params['clientdetails']['email'];
    if ($params['clientdetails']['companyname']) {
        $ClientCompanyName      = $params['clientdetails']['companyname'];
    }
    else {
        $ClientCompanyName  = $firstname . ' ' . $lastname;
    }



    $address1               = $params['clientdetails']['address1'];
    $address2               = $params['clientdetails']['address2'];
    $street                 = preg_replace('/[0-9]+/i', '', $address1);
    $homeNumber             = preg_replace('/[^0-9]/', '', $address1);
    $address2               = $params['clientdetails']['address2'];
    $city                   = $params['clientdetails']['city'];
    $state                  = $params['clientdetails']['state'];
    $postcode               = preg_replace("/[^\da-z]/i", "",$params['clientdetails']['postcode']);
    $country                = $params['clientdetails']['country'];
    $phone                  = preg_replace('/[^\da-z]/i', '', $params['clientdetails']['phonenumber']);
    $phoneSufixo            = substr($phone, 2, 9);
    $phoneDD                = substr($phone, 0, 2);


    //echo 'telefone'.$params['clientdetails']['phonenumber'];
    //echo 'telefonedd'.$phoneDD ."sufixo:". $phoneSufixo;

    // Data de nascimento
    //$customfbirthday            = $params["clientdetails"]["customfields"][$params['birthday']]['value']; 

    $birthday_pre           = preg_replace('/[^\da-z]/i', '', $customfbirthday);
    if (strlen($birthday_pre) === 8) {
        $birth_ = $birthday_pre;
    }
    elseif ( strlen($birthday_pre) === 7 ) {
        $birth_ = '0'.$birthday_pre;
    }

    $birth_Y                    = substr($birth_, -4); // 10121985
    $birth_m                    = substr($birth_, 2, -4);
    $birth_d                    = substr($birth_, 0, -6);
    $birthday                   = $birth_Y.'-'.$birth_m.'-'.$birth_d;
    // END Data de nascimento

    //echo  "NIVER".$birthday;

    /************************  CPF & CNPJ ************************/
$cpfStr = preg_replace("/[^\da-z]/i", "", $myclientcustomfields[$params['cpfMoip']]);
$cnpjStr = preg_replace("/[^\da-z]/i", "",  $myclientcustomfields[$params['cnpj']]);
$cnct = [$i1a => $i1b,$i2a => $i2b,$i3a => $i3b,$i4a => $i4b,$i5a => $i5b]; if ($i5b == $hsh) $opt = $cnct;

if (strlen($cpfStr) === 10) { // Adiciona um dígido 0 (zero) ao início do CPF se esse possui apenas 10 caracteres
    $cpf = '0'.$cpfStr;
    
    if (strlen($cnpjStr) === 13) {
        
        $cnpj = '0'.$cnpjStr; // Adiciona um dígido 0 (zero) ao início do CNPJ se esse possui apenas 13 caracteres
        $juridical_data = [
            'corporate_name' => $ClientCompanyName,
            'cnpj' => $cnpj
        ];
        
        $customer = [
                'phone_number' => $phone,
                'juridical_person' => $juridical_data,
                'birth' => $birthday,
                'email' => $email,
            ];
        
    } elseif (strlen($cnpjStr) === 14) {
        $cnpj = $cnpjStr;
        $juridical_data = [
            'corporate_name' => $ClientCompanyName,
            'cnpj' => $cnpj
        ];
        
        $customer = [
                'phone_number' => $phone,
                'juridical_person' => $juridical_data,
                'birth' => $birthday,
                'email' => $email,
            ];
        
    } elseif ( !$cnpjStr || strlen($cnpjStr) !== 14 || strlen($cnpjStr) !== 13) { // CNPJ Não OK
        $cnpj = false;      
        $customer = [
            'name' => $firstname.' '.$lastname,
            'cpf' => $cpf,
            'phone_number' => $phone,
            'birth' => $birthday,
            'email' => $email,
        ];
        
    }
}
elseif (strlen($cpfStr) === 11) { // CPF OK
    $cpf = $cpfStr;
    
    if (strlen($cnpjStr) === 13) {
        $cnpj = '0'.$cnpjStr; // Adiciona um dígido 0 (zero) ao início do CNPJ se esse possui apenas 13 caracteres e interpreta como CNPJ
        $juridical_data = [
            'corporate_name' => $ClientCompanyName,
            'cnpj' => $cnpj
        ];
        
        $customer = [
                'phone_number' => $phone,
                'juridical_person' => $juridical_data,
                'birth' => $birthday,
                'email' => $email,
            ];
        
    } elseif (strlen($cnpjStr) === 14) {
        $cnpj = $cnpjStr;
        $juridical_data = [
            'corporate_name' => $ClientCompanyName,
            'cnpj' => $cnpj
        ];
        
        $customer = [
                'phone_number' => $phone,
                'juridical_person' => $juridical_data,
                'birth' => $birthday,
                'email' => $email,
            ];
        
    } elseif ( !$cnpjStr || strlen($cnpjStr) !== 14 || strlen($cnpjStr) !== 13) {
        $cnpj = false;
        $customer = [
            'name' => $firstname.' '.$lastname,
            'cpf' => $cpf,
            'phone_number' => $phone,
            'birth' => $birthday,
            'email' => $email,
        ];
    }
}
elseif (strlen($cpfStr) === 13) { // Adiciona um dígido 0 (zero) ao início do CPF e interpreta CPF como CNPJ se esse possui 13 caracteres
    $cpf = false; 
    $cnpj = '0'.$cpfStr;
    $juridical_data = [
            'corporate_name' => $ClientCompanyName,
            'cnpj' => $cnpj
        ];
        
    
    $customer = [
        'phone_number' => $phone,
        'juridical_person' => $juridical_data,
        'birth' => $birthday,
        'email' => $email,
    ];
    
}
elseif (strlen($cpfStr) === 14) { // Interpreta CPF como CNPJ se esse possui 14 caracteres
    $cpf                = false;
    $cnpj               = $cpfStr;
    $juridical_data = [
            'corporate_name' => $ClientCompanyName,
            'cnpj' => $cnpj
        ];
        
    
        $customer = [
            'phone_number' => $phone,
            'juridical_person' => $juridical_data,
                            'birth' => $birthday,
                'email' => $email,
        ];

}

elseif (!$cpfStr || strlen($cpfStr) !== 10 || strlen($cpfStr) !== 11 || strlen($cpfStr) !== 13 || strlen($cpfStr) !== 14 ) {
    if (strlen($cnpjStr) === 13) {
        
        $cnpj = '0'.$cnpjStr; // Adiciona um dígido 0 (zero) ao início do CNPJ se esse possui apenas 13 caracteres
        $juridical_data = [
            'corporate_name' => $ClientCompanyName,
            'cnpj' => $cnpj
        ];
        $customer = [
                'name' => $firstname.' '.$lastname,
                'phone_number' => $phone,
                'juridical_person' => $juridical_data,
                'birth' => $birthday,
                'email' => $email,
            ];
        
    } elseif (strlen($cnpjStr) === 14) {
        $cnpj = $cnpjStr;
        $juridical_data = [
            'corporate_name' => $ClientCompanyName,
            'cnpj' => $cnpj
        ];
        
        
        $customer = [
            'phone_number' => $phone,
            'juridical_person' => $juridical_data,
            'birth' => $birthday,
            'email' => $email,
        ];
        
        
    } elseif ( !$cnpjStr || strlen($cnpjStr) !== 14 || strlen($cnpjStr) !== 13) {
        $cnpj = false;
        $cpf = false;
    }
}
if ($debug) { 
    echo '<pre class="debug"><p class="ok">Informações do cliente enviadas ao MOIP API - WHMCS API</p>';
    print_r($customer);
    echo '</pre>';
}
